  function validateEmail(email){
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (reg.test(email)){
    return true; }
    else{
    return false;
    }
  }
    $(document).ready(function(){
      $("#registerBtn").click(function(){
        var regAs = $("#as").val();
        var email = $("#reg_email").val();
        var pwd = $("#reg_pwd").val();
        var cn_pwd = $("#reg_pwd_cnf").val();
        var fname = $("#reg_fname").val();
        var gender = $("#gender").val();
        var captcha = $("#cap_code").val();
        var terms = $("#id_tos").is(':checked');
        var errors = 0;
        if(regAs == 0){
        $('#reg_text').addClass('redText');
        errors = 1;
        }
        else
        $('#reg_text').removeClass('redText');

        if(!validateEmail(email) || email==''){
          errors = 1;
          $('#email_text').addClass('redText');
          $('#reg_email_error').removeClass('d-none');
        }
        else{
          $('#reg_email_error').addClass('d-none');
          $('#email_text').removeClass('redText');
        }

        if(pwd.length < 8){
          errors = 1;
          $('#reg_pwd_error').removeClass('d-none');
          $('#pwd_text, #cn_pwd_text').addClass('redText');
        }
        else{
          $('#reg_pwd_error').addClass('d-none');
          $('#pwd_text, #cn_pwd_text').removeClass('redText');
        }

        if(fname == "" || fname == ' '){
          errors = 1;
          $('#reg_fname_error').removeClass('d-none');
          $('#fname_text').addClass('redText');
        }
        else{
          $('#reg_fname_error').addClass('d-none');
          $('#fname_text').removeClass('redText');
        }

        if(gender == 0){
          errors = 1;
          $('#reg_gender_error').removeClass('d-none');
          $('#gender_text').addClass('redText');
        }
        else{
          $('#reg_gender_error').addClass('d-none');
          $('#gender_text').removeClass('redText');
        }
        
        if(captcha !== 'Y00@bc'){
          errors = 1;
          $('#reg_cap_error').removeClass('d-none');
          $('#cap_text').addClass('redText');
        }
        else{
          $('#reg_cap_error').addClass('d-none');
          $('#cap_text').removeClass('redText');
        }

        if(errors == 0)
        alert('Registered Successfully!');         
      });
      $("#cancelButton").click(function(){
        $("#as, #gender").val(0);
        $("#reg_email, #reg_pwd, #reg_pwd_cnf, #reg_fname, #cap_code, #reg_mname, #reg_lname, #reg_dname, #reg_mno").val('');
        $("#id_tos").prop('checked',false);
        $('#reg_text, #email_text, #pwd_text, #cn_pwd_text, #fname_text, #gender_text, #cap_text').removeClass('redText');
        $('#reg_email_error, #reg_pwd_error, #reg_fname_error, #reg_gender_error, #reg_cap_error').addClass('d-none');
      });
    });
